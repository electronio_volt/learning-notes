### _Introduction to Philosophy_ 
#### Coursera course notes

**Philosophy branches**
* **Utilitarianism:** One of the best known and most influential moral theories. Like other forms of consequentialism, its core idea is that whether actions are morally right or wrong depends on their effects [^1].

**Books**
* _Tractatus Logico-Philosophicus_, Ludwig Wittgenstein: Relationship between language and reality, definition of the limits of science


[^1]: Internet Encyclopedia of Philosophy (https://iep.utm.edu/)